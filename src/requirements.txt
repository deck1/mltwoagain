Django==2.2
boto3
django-cors-headers
djongo
djangorestframework
djangorestframework-jsonapi
sqlparse==0.2.4
mongoengine
django-rest-framework-mongoengine
pyjwt
requests
nbformat
ipython
psycopg2
numpy
scikit-learn
redis
pandas
xgboost
pickle5
imblearn
matplotlib
seaborn
fpdf2
multiprocess
joblib
dask-ml
dask-searchcv
xgboost